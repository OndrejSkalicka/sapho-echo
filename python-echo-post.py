import sys
from collections import deque
from datetime import datetime
from http.server import HTTPServer, BaseHTTPRequestHandler


class RequestLog:

    def __init__(self, method: str, path: str, headers: [str], payload: str = None) -> None:
        self.time = datetime.now().isoformat()
        self.method = method
        self.path = path
        self.headers = headers
        self.payload = payload

    def as_html(self):
        nice_headers = "<br/>".join('<strong>%s:</strong>%s' % (h, self.headers.get(h)) for h in self.headers)
        return '''
<tr><td>%s</th><th class="code">%s</th><td class="code">%s</td><td>%s</td><td class="code pre">%s</td>''' % (
            self.time, self.method, self.path, nice_headers,
            '%d byte(s):<hr/>%s' % (len(self.payload), self.payload) if self.payload else "None")


class RequestHandler(BaseHTTPRequestHandler):
    log_size = 50
    log = deque(maxlen=log_size)

    def do_GET(self):
        if self.path == '/favicon.ico':
            self.send_error(404)
            return

        if self.path == '/log':
            self.respond_html('Log (last %d requests)' % self.log_size,
                              '''
<table>
<tr><th>Time</th><th>Method</th><th>path</th><th>headers</th><th>payload</th></tr>
%s</table>''' % ''.join(log.as_html() for log in reversed(self.log)))
            return

        self.log.append(RequestLog("GET", self.path, self.headers))
        self.respond_html('OK', '<h1>GET OK!</h1>')

    def do_POST(self):
        content_length = self.headers.get('Content-Length')
        length = int(content_length) if content_length else 0
        payload = self.rfile.read(length).decode("utf-8")

        self.log.append(RequestLog("POST", self.path, self.headers, payload))
        self.respond_html('OK', '<h1>POST OK!</h1>')

    def do_PUT(self):
        content_length = self.headers.get('Content-Length')
        length = int(content_length) if content_length else 0
        payload = self.rfile.read(length).decode("utf-8")

        self.log.append(RequestLog("PUT", self.path, self.headers, payload))
        self.respond_html('OK', '<h1>PUT OK!</h1>')

    def respond_html(self, title: str, body: str):

        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes('''<!doctype html>
<html lang=en>
<head>
<meta charset=utf-8>
<title>%s</title>
<style type="text/css"> 
td, th {vertical-align: top; padding-bottom: 15px}
.code {font-family: "Courier New", Courier, monospace}
.pre {white-space: pre;} 
</style>
</head>
<body>%s</body>
</html>''' % (title, body), "utf-8"))


def main():
    port = int(sys.argv[1]) if len(sys.argv) >= 2 else 8080
    host = sys.argv[2] if len(sys.argv) >= 3 else ''

    server = HTTPServer((host, port), RequestHandler)

    print(datetime.now().isoformat(), "Server Starts - %s:%s" % (host, port))
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
    print(datetime.now().isoformat(), "Server Stops - %s:%s" % (host, port))


if __name__ == "__main__":
    main()
