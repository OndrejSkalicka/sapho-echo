do this to run it (for the first time, also do a `heroku login`):

```
git clone https://gitlab.com/OndrejSkalicka/sapho-echo.git
cd sapho-echo
heroku create --buildpack heroku/python
git push heroku master
heroku config:set PORT=8080
heroku ps:scale web=1
heroku open
```

see https://elements.heroku.com/buildpacks/heroku/heroku-buildpack-python for buildpack info.